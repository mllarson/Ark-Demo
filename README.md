Running Instructions:

Download Ark_Demo-Start.jar and run the command
java -jar {filepath}/Ark_Demo-Start.jar

The application will launch and start listening on localhost:8080

Use curl, a REST Api tesing application like Postman or the swagger page at http://localhost:8080/swagger-ui/index.html for testing endpoints

For creation only a name is required for Funds and Investors, their ID is returned when creating or updating successfully.

Transactions require a type that must be "Contribution" "Interest Income" "Distribution" "General Expense" or "Management Fee" as well as an amount and request parameters that correspond an Existing Investor and Fund. Credit transactions must have a positive amount and Debit must have a Negative amount. Failed requests recieve a 400 error and a short explanation.

Once Investors Funds and Transactions have been created total balance of an Investor or Fund can be queried as well as the list of investors on a fund or funds contributed to by an investor, as well as transactions associated with any specific fund or investor.

Check the swagger ui for specifics of the API calls.
